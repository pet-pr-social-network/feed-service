module gitlab.com/pet-pr-social-network/feed-service

go 1.20

require (
	github.com/grpc-ecosystem/go-grpc-middleware v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/redis/go-redis/v9 v9.0.5
	github.com/rs/zerolog v1.29.1
	github.com/segmentio/kafka-go v0.4.41-0.20230519172151-199b8b2e0897
	github.com/stretchr/testify v1.8.4
	gitlab.com/pet-pr-social-network/post-service v0.0.0-20230605170712-906e3ae055a8
	gitlab.com/pet-pr-social-network/relation-service v0.0.0-20230605154209-a4ba2324e44e
	google.golang.org/grpc v1.55.0
	google.golang.org/protobuf v1.30.0
)

require (
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/klauspost/compress v1.16.5 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/pierrec/lz4/v4 v4.1.17 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.5.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20230530153820-e85fd2cbaebc // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
